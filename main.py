import sys
import time
import random
import numpy as np

from PySide2.QtWidgets import (QLineEdit, QPushButton, QApplication, QDialog, QMessageBox, QLabel, QGridLayout)
from PySide2 import QtGui
from PySide2 import QtCore

# BUUGS
# change direction state while updating might cause wrong game over -> lcok direction while changestate


snakeArt = r"""
       ---_ ......._-_--.
      (|\ /      / /| \  \
      /  /     .'  -=-'   `.
     /  /    .'             )
   _/  /   .'        _.)   /
  / o   o        _.-' /  .'
  \          _.-'    / .'*|
   \______.-'//    .'.' \*|
    \|  \ | //   .'.' _ |*|
     `   \|//  .'.'_ _ _|*|
      .  .// .'.' | _ _ \*|
      \`-|\_/ /    \ _ _ \*\
       `/'\__/      \ _ _ \*\
      /^|            \ _ _ \*
     '  `             \ _ _ \
                       \_
Welcome to pySnake
"""
print(snakeArt)

ROW_COUNT = 15
COLUMN_COUNT = 15
grid = np.zeros((ROW_COUNT,COLUMN_COUNT))

# set random seed (take systen time)
random.seed(None)


# apple should only spawn where empty (0)
def validate_apple_pop(random_row,random_col):
    new_apple_pop_cell = grid[random_row][random_col]
    if new_apple_pop_cell == 0:
        return True
    else:
        return False

def get_random_row():
    return random.randrange(0,ROW_COUNT)
def get_random_col():
    return random.randrange(0,COLUMN_COUNT)

def pop_apple():
    grid_apple = np.where(grid == -1)
    # grid apple not found if
    if not grid_apple[0].size > 0:
        print("POPPING")
        valid_pop_location = False
        while not valid_pop_location:
            random_row = get_random_row()
            random_col = get_random_col()
            valid_pop_location = validate_apple_pop(random_row,random_col)
        # set apple
        grid[random_row][random_col] = -1
        # increment speed and score
        global speed
        global score
        speed = speed - 0.01
        highest_value_cell = get_highest_value_cell()
        highest_value_cell_value = grid[highest_value_cell[0]][highest_value_cell[1]]
        score = highest_value_cell_value

# check if move valid eg, only right left if down etc.
def validate_move(current_direction, input_direction):
    if current_direction == "Right" or current_direction == "Left":
        if input_direction == "Up" or input_direction == "Down":
            return True
        else:
            return False
    elif current_direction == "Up" or current_direction == "Down":
        if input_direction == "Right" or input_direction == "Left":
            return True
        else:
            return False

def get_next_cell_coords(input_direction,snake_head_coords):

    # check for game over here too

    if input_direction == "Left":
        next_row = snake_head_coords[0][0]
        next_col = snake_head_coords[1][0]-1
        next_cell_coords = [next_row,next_col]
    elif input_direction == "Right":
        next_row = snake_head_coords[0][0]
        next_col = snake_head_coords[1][0]+1
        next_cell_coords = [next_row,next_col]
    elif input_direction == "Up":
        next_row = snake_head_coords[0][0]-1
        next_col = snake_head_coords[1][0]
        next_cell_coords = [next_row,next_col]
    elif input_direction == "Down":
        next_row = snake_head_coords[0][0]+1
        next_col = snake_head_coords[1][0]
        next_cell_coords = [next_row,next_col]

    return next_cell_coords

def get_snake_head_cell():
    snake_head_cell = np.where(grid == 1)
    snake_head_cell = list(zip(snake_head_cell[0], snake_head_cell[1]))
    snake_head_cell = [snake_head_cell[0][0], snake_head_cell[0][1]]
    return snake_head_cell

def get_highest_value_cell():
    highest_value_cell = np.where(grid == np.amax(grid))
    highest_value_cell = list(zip(highest_value_cell[0], highest_value_cell[1]))
    highest_value_cell = [highest_value_cell[0][0], highest_value_cell[0][1]]
    return highest_value_cell


def get_snake_cells():
    snake_cells = np.where(grid >= 1)
    snake_cells = list(zip(snake_cells[0], snake_cells[1]))
    return snake_cells

def increment_snake_cells():
    snake_cells = get_snake_cells()
    for i in range(len(snake_cells)):
        grid[snake_cells[i][0]][snake_cells[i][1]] = grid[snake_cells[i][0]][snake_cells[i][1]] + 1
    
def get_apple_cell_coords():
    return

def is_game_over(next_cell_coords):
    # first, check boundaries, then check for snake himself ;)
    if next_cell_coords[0] < 0 or next_cell_coords[0] > ROW_COUNT-1 or next_cell_coords[1] < 0 or next_cell_coords[1] > COLUMN_COUNT-1:
        return True
    else:
        next_cell_value = grid[next_cell_coords[0]][next_cell_coords[1]]
        if next_cell_value >= 1:
            return True
        else:
            return False

def move_snake(grid, direction):
    global is_alive
    print(grid)
    print(direction)

    snake_head_coords = np.where(grid == 1)

    snake_head_cell = get_snake_head_cell()
    print('snake_head_coords(1) : ', snake_head_coords)


    if direction == "Left":
        next_cell_coords = get_next_cell_coords(direction,snake_head_coords)
        # check gameover
        game_over = is_game_over(next_cell_coords)
        if game_over:
            is_alive = False
            return


        next_cell_value = grid[next_cell_coords[0]][next_cell_coords[1]]
        print("NEXTCELL",next_cell_value)
        if next_cell_value == -1:
            next_cell_is_apple = True
        else:
            next_cell_is_apple = False
        
        if not next_cell_is_apple:
            # make highest zero
            highest_value_cell = get_highest_value_cell()
            grid[highest_value_cell[0]][highest_value_cell[1]] = 0
        

        # make all greater EQUAL 1 plus 1
        increment_snake_cells()

        grid[snake_head_coords[0][0]][snake_head_coords[1][0]-1] = 1



    if direction == "Right":
        next_cell_coords = get_next_cell_coords(direction,snake_head_coords)
        game_over = is_game_over(next_cell_coords)
        if game_over:
            is_alive = False
            return
        next_cell_value = grid[next_cell_coords[0]][next_cell_coords[1]]
        print("NEXTCELL",next_cell_value)
        if next_cell_value == -1:
            next_cell_is_apple = True
        else:
            next_cell_is_apple = False
        
        if not next_cell_is_apple:
            # make highest zero
            highest_value_cell = get_highest_value_cell()
            grid[highest_value_cell[0]][highest_value_cell[1]] = 0
        

        # make all greater EQUAL 1 plus 1
        increment_snake_cells()
        
        grid[snake_head_coords[0][0]][snake_head_coords[1][0]+1] = 1

    
    if direction == "Up":
        next_cell_coords = get_next_cell_coords(direction,snake_head_coords)
        game_over = is_game_over(next_cell_coords)
        if game_over:
            is_alive = False
            return

        next_cell_value = grid[next_cell_coords[0]][next_cell_coords[1]]
        print("NEXTCELL",next_cell_value)
        if next_cell_value == -1:
            next_cell_is_apple = True
        else:
            next_cell_is_apple = False
        
        if not next_cell_is_apple:
            # make highest zero
            highest_value_cell = get_highest_value_cell()
            grid[highest_value_cell[0]][highest_value_cell[1]] = 0
        

        # make all greater EQUAL 1 plus 1
        increment_snake_cells()

        grid[snake_head_coords[0][0]-1][snake_head_coords[1][0]] = 1

    if direction == "Down":
        next_cell_coords = get_next_cell_coords(direction,snake_head_coords)
        game_over = is_game_over(next_cell_coords)
        if game_over:
            is_alive = False
            return
        next_cell_value = grid[next_cell_coords[0]][next_cell_coords[1]]
        print("NEXTCELL",next_cell_value)
        if next_cell_value == -1:
            next_cell_is_apple = True
        else:
            next_cell_is_apple = False
        
        if not next_cell_is_apple:
            # make highest zero
            highest_value_cell = get_highest_value_cell()
            grid[highest_value_cell[0]][highest_value_cell[1]] = 0
        
        # make all greater EQUAL 1 plus 1
        increment_snake_cells()

        grid[snake_head_coords[0][0]+1][snake_head_coords[1][0]] = 1


    pop_apple()







class snakeWorker(QtCore.QThread):
    def __init__(self):
        QtCore.QThread.__init__(self)

    def __del__(self):
        self.wait()

    def run(self):
        global direction
        global is_alive
        global speed
        print (is_alive)
        if is_alive:
            move_snake(grid, direction)
            time.sleep(speed)
        else:
            self.exit



class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.setGeometry(500, 500, 800, 600)
        self.setWindowTitle("pySnake")

        # Create widgets
        self.edit = QLineEdit("Nickname")
        self.button = QPushButton("Start")
        self.lbl_score_help = QLabel("Current score:")
        self.lbl_score = QLabel("")
        self.lbl_speed_help = QLabel("Speed:")
        self.lbl_speed = QLabel("")

        # Create layout and add widgets
        # layout = QVBoxLayout()
        layout = QGridLayout()
        
        layout.addWidget(self.lbl_score_help,0,1,1,1)
        layout.addWidget(self.lbl_score,1,1,1,1)
        layout.addWidget(self.lbl_speed_help,2,1,1,1)
        layout.addWidget(self.lbl_speed,3,1,1,1)
        layout.addWidget(self.edit,10,0,1,1)
        layout.addWidget(self.button,11,0,1,1)
        # layout.setAlignment(QtCore.Qt.AlignBottom)
        # Set dialog layout
        self.setLayout(layout)

        #Add button signal to greetings slot
        self.button.clicked.connect(self.startGame)

    # Greets the user
    def startGame(self):
        print ("Hello {}, you started pySnake - have fun!".format(self.edit.text()))

        #lock nickname and start button
        self.edit.setDisabled(True)
        self.button.setDisabled(True)

        #try to make initial snake state
        global grid
        global is_alive
        global speed
        global direction
        global score
        score = 3
        direction = "Right"
        speed = 0.5
        is_alive = True
        grid = np.zeros((ROW_COUNT,COLUMN_COUNT))
        grid[5][6] = 1
        grid[5][5] = 2
        grid[5][4] = 3

        self.runThread()



    def keyPressEvent(self, e):
        global direction
        print(e)
        key = e.key()

        if key == QtCore.Qt.Key_Escape:
            self.close()
        elif key == QtCore.Qt.Key_Left:
            input_direction = "Left"
            validity = validate_move(direction,input_direction)
            if validity:
                direction = input_direction
        elif key == QtCore.Qt.Key_Right:
            input_direction = "Right"
            validity = validate_move(direction,input_direction)
            if validity:
                direction = input_direction
        elif key == QtCore.Qt.Key_Down:
            input_direction = "Down"
            validity = validate_move(direction,input_direction)
            if validity:
                direction = input_direction
        elif key == QtCore.Qt.Key_Up:
            input_direction = "Up"
            validity = validate_move(direction,input_direction)
            if validity:
                direction =input_direction
            
            



    def runThread(self):
        global is_alive
        global speed
        global score
        if is_alive:
            self.lbl_speed.setText(str(speed))
            self.lbl_score.setText(str(score))
            self.update()
            self.thread = snakeWorker()
            self.thread.finished.connect(self.runThread)
            self.thread.start()
        else:
            print("GAME OVER")
            msgBox = QMessageBox()
            msgBox.setText("Game Over.")
            msgBox.exec()

            #save score??


            # make new grid again


            #unlock nickname and start button
            self.edit.setDisabled(False)
            self.button.setDisabled(False)



    def paintEvent(self, e):
        qp = QtGui.QPainter()
        qp.begin(self)
        self.drawRectangles(qp)
        qp.end()
        
    def drawRectangles(self, qp):
        color = QtGui.QColor(0, 0, 0)
        color.setNamedColor('#d4d4d4')
        qp.setPen(color)
        
        
        w = 30
        x,y = 0,0

        for row in grid:
            for col in row:
                if col >= 1:
                    qp.setBrush(QtGui.QColor(0, 0, 0))
                elif col == 0:
                    qp.setBrush(QtGui.QColor(255, 255, 255))
                elif col == -1:
                    qp.setBrush(QtGui.QColor(255, 0, 0))
                qp.drawRect(x, y, 30, 30)
                x = x + w
            y = y + w
            x = 0


if __name__ == '__main__':
    # Create the Qt Application
    app = QApplication(sys.argv)
    # Create and show the form
    form = Form()
    form.show()
    # Run the main Qt loop
    sys.exit(app.exec_())


