import random
import time
from enum import Enum
import keyboard


class Cell:
    celltype = None

    def __init__(self, row, col, celltype):
        self.row = row
        self.col = col
        self.celltype = celltype


class CellType(Enum):
    EMPTY = 1,
    FOOD = 2,
    SNAKE_NODE = 3


class Snake:
    snake_part_list = []
    head = None

    def __init__(self, init_pos):
        self.head = init_pos
        self.snake_part_list.append(self.head)

    def grow(self):
        self.snake_part_list.append(self.head)

    def move(self, next_cell):
        print("Snake is moving to: ", next_cell.row, next_cell.col)
        tail = self.snake_part_list.pop()
        tail.celltype = CellType.EMPTY
        self.head = next_cell
        self.head.celltype = CellType.SNAKE_NODE
        self.snake_part_list.insert(0, self.head)

    def check_crash(self, next_cell):
        print("Going to check for crash")
        for cell in self.snake_part_list:
            if cell == next_cell:
                return True


class Board:
    ROW_COUNT = None
    COL_COUNT = None
    cells = None
    
    def __init__(self, row_count, col_count):
        self.ROW_COUNT = row_count
        self.COL_COUNT = col_count

        self.cells = [[None for x in range(self.ROW_COUNT)] for y in range(self.COL_COUNT)] 
        for row in range(self.ROW_COUNT):
            for col in range(self.COL_COUNT):
                self.cells[row][col] = Cell(row, col, CellType.EMPTY)
        
    def generate_food(self):
        print("Going to generate food")
        random_row = random.randrange(0, self.ROW_COUNT)
        random_col = random.randrange(0, self.COL_COUNT)

        self.cells[random_row][random_col].celltype = CellType.FOOD

        print("Food is generated at: ", random_row,random_col)


class Game:
    DIRECTION_NONE, DIRECTION_RIGHT, DIRECTION_LEFT, DIRECTION_UP, DIRECTION_DOWN = 0, 1, -1, 2, -2
    random.seed(None)

    snake = None
    board = None
    direction = None
    game_over = None

    def __init__(self, snake, board):
        self.snake = snake
        self.board = board

    def update(self):
        print("Going to update the game")
        if not self.game_over:
            if not self.direction == self.DIRECTION_NONE:
                next_cell =  self.get_next_cell(self.snake.head)
                print("Found next cell: ", vars(next_cell))

                if self.snake.check_crash(next_cell):
                    self.direction = self.DIRECTION_NONE
                    self.game_over = True
                else:
                    print(vars(next_cell))
                    if next_cell.celltype == CellType.FOOD:
                        print("__________GROWING___________")
                        self.snake.grow()
                        self.board.generate_food() 
                    
                    self.snake.move(next_cell)
                

    def get_next_cell(self, current_position):
        print("Going to find next cell")
        row = current_position.row
        col = current_position.col

        if self.direction == self.DIRECTION_RIGHT:
            col += 1
        if self.direction == self.DIRECTION_LEFT:
            col -= 1
        if self.direction == self.DIRECTION_UP:
            row -= 1
        if self.direction == self.DIRECTION_DOWN:
            row += 1
            
        next_cell = self.board.cells[row][col]
        return next_cell
        

    def render(self):
        for row in range(self.board.ROW_COUNT):
            for col in range(self.board.COL_COUNT):
                if self.board.cells[row][col].celltype == CellType.EMPTY:
                   print("[ ]", end='\t')
                elif self.board.cells[row][col].celltype == CellType.FOOD:
                   print("[O]", end='\t')
                elif self.board.cells[row][col].celltype == CellType.SNAKE_NODE:
                   print("[X]", end='\t')
            print('\n')

        for i in self.snake.snake_part_list:
            print(self.snake.snake_part_list)

        


def main():
    print("Going to start a game!")
    init_pos = Cell(0, 0, CellType.SNAKE_NODE)
    init_snake = Snake(init_pos)
    board = Board(10, 10)
    new_game = Game(init_snake, board)
    new_game.game_over = False
    new_game.direction = 1

    new_game.board.generate_food()
    for row in range(board.ROW_COUNT):
        for col in range(board.ROW_COUNT):
            print(vars(board.cells[row][col]))

    while not new_game.game_over == True:
        new_game.update()
        new_game.render()
        time.sleep(1)
        # clear console
        # print(chr(27) + "[2J")
        
        if keyboard.is_pressed('s'):
            new_game.direction = new_game.DIRECTION_DOWN
        elif keyboard.is_pressed('w'):
            new_game.direction = new_game.DIRECTION_UP
        elif keyboard.is_pressed('d'):
            new_game.direction = new_game.DIRECTION_RIGHT
        elif keyboard.is_pressed('a'):
            new_game.direction = new_game.DIRECTION_LEFT

if __name__ == "__main__":
    main()
